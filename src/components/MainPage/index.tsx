import * as React from "react";
import {
    Container,
    Divider,
    Grid,
    Header,
    Segment
} from "semantic-ui-react";
import ApiClientService from "../../services/api-client-service";
import {CovidStateCount, CovidStateInfo, UsCovidCount, UsCovidDailyCount} from "../../models/api-models";

interface PageProps {
}

interface PageState {
    covidStateCounts: CovidStateCount[];
    covidStateInfo: CovidStateInfo[];
    covidUsCounts: UsCovidCount[];
    covidUsDailyCounts: UsCovidDailyCount[];
}

class Index extends React.Component<PageProps, PageState> {

    private client: ApiClientService;

    constructor(props: PageProps, state: PageState) {
        super(props, state);

        this.client = new ApiClientService();

        this.state = {
            covidUsCounts: [],
            covidStateCounts: [],
            covidStateInfo: [],
            covidUsDailyCounts: []
        };
    }

    componentDidMount(): void {
        this.initData();
    }

    initData = () => {
        this.client.getUsCurrentCases()
            .then((response: UsCovidCount[]) => {
                this.setState({covidUsCounts: response});
            });
        this.client.getCurrentStateCases()
            .then( (response: CovidStateCount[]) => {
               console.log(response);
               this.setState({covidStateCounts: response});
            });
        this.client.getStateCovidInfo()
            .then( (response: CovidStateInfo[]) => {
               console.log(response);
               this.setState({covidStateInfo: response});
            });
    };

    getUSCount = (): UsCovidCount => {
        return this.state.covidUsCounts[0] || 0;
    };

    formatNumberValue = (value: number) => {
        return new Intl.NumberFormat().format(value);
    };

    public render() {
        return (
            <Container>
                <Grid>
                    <Grid.Row>
                        <Grid.Column computer={16}>
                            <Header as={"h1"} textAlign={"center"}>
                                Covid-19 Stats
                            </Header>
                            <Divider/>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                        <Grid.Column computer={16}>
                            <Segment.Group>
                                <Segment>
                                    <Header>U.S. Current Cases</Header>
                                    <Segment.Group horizontal={true}>
                                        <Segment textAlign={'center'}>
                                            Positive: {this.formatNumberValue(this.getUSCount().positive)}
                                        </Segment>
                                        <Segment textAlign={'center'}>
                                            Negative: {this.formatNumberValue(this.getUSCount().negative)}
                                        </Segment>
                                        <Segment textAlign={'center'}>
                                            Possible Negatives: {this.formatNumberValue(this.getUSCount().posNeg)}
                                        </Segment>
                                        <Segment textAlign={'center'}>
                                            Hospitalized: {this.formatNumberValue(this.getUSCount().hospitalized)}
                                        </Segment>
                                        <Segment textAlign={'center'}>
                                            Deaths: {this.formatNumberValue(this.getUSCount().death)}
                                        </Segment>
                                        <Segment textAlign={'center'}>
                                            Total: {this.formatNumberValue(this.getUSCount().total)}
                                        </Segment>
                                    </Segment.Group>
                                </Segment>
                            </Segment.Group>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Container>
        );
    }
}

export default Index;
