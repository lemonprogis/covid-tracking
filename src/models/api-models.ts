export interface CovidStateCount {
    state: string;
    positive: number;
    positiveScore: number;
    negativeScore: number;
    negativeRegularScore: number;
    commercialScore: number;
    grade: string;
    score: number;
    negative: number;
    pending?: any;
    hospitalized: number;
    death?: any;
    total: number;
    lastUpdateEt: string;
    checkTimeEt: string;
    dateModified: Date;
    dateChecked: Date;
}

export interface CovidStateInfo {
    state: string;
    covid19SiteOld: string;
    covid19Site: string;
    covid19SiteSecondary: string;
    twitter: string;
    pui: string;
    pum: boolean;
    notes: string;
    name: string;
}

export interface UsCovidCount {
    positive: number;
    negative: number;
    posNeg: number;
    hospitalized: number;
    death: number;
    total: number;
}

export interface UsCovidDailyCount {
    date: number;
    states: number;
    positive: number;
    negative: number;
    posNeg: number;
    pending: number;
    hospitalized?: any;
    death?: any;
    total: number;
}
