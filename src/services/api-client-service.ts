import axios from 'axios';
import {CovidStateCount, CovidStateInfo, UsCovidCount, UsCovidDailyCount} from "../models/api-models";

export default class ApiClientService {
    private baseUrl: string = `https://covidtracking.com`;

    getCurrentStateCases = (): Promise<CovidStateCount[]> => {
        const url = this.urlBuilder(`/api/states`);

        return new Promise((resolve, reject) => {
            axios.get(url)
                .then(({ status, data} ) => {
                    if (status === 200) {
                        resolve(data);

                    } else {
                        reject(new Error('error fetching data.'));
                    }
                });
        });
    };

    getCurrentCasesByState = (state: string): Promise<CovidStateCount[]> => {
        const url = this.urlBuilder(`/api/states`);

        return new Promise((resolve, reject) => {
            axios.get(url, { params: {state: state}})
                .then(({ status, data} ) => {
                    if (status === 200) {
                        resolve(data);

                    } else {
                        reject(new Error('error fetching data.'));
                    }
                });
        });
    };

    getUsCurrentCases = (): Promise<UsCovidCount[]> => {
        const url = this.urlBuilder(`/api/us`);

        return new Promise((resolve, reject) => {
            axios.get(url)
                .then(({ status, data} ) => {
                    if (status === 200) {
                        resolve(data);

                    } else {
                        reject(new Error('error fetching data.'));
                    }
                });
        });
    };

    getUsDailyCounts = (): Promise<UsCovidDailyCount[]> => {
        const url = this.urlBuilder(`/api/us/daily`);

        return new Promise((resolve, reject) => {
            axios.get(url)
                .then(({ status, data} ) => {
                    if (status === 200) {
                        resolve(data);

                    } else {
                        reject(new Error('error fetching data.'));
                    }
                });
        });
    };

    getStateCovidInfo = (): Promise<CovidStateInfo[]> => {
        const url = this.urlBuilder(`/api/states/info`);

        return new Promise((resolve, reject) => {
            axios.get(url)
                .then(({ status, data} ) => {
                    if (status === 200) {
                        resolve(data);

                    } else {
                        reject(new Error('error fetching data.'));
                    }
                });
        });
    };

    private urlBuilder = (url: string) => {
      return `${this.baseUrl}${url}`;
    };
}
